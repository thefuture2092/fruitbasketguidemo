package ca.ulaval.ift.gloo.view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;

@SuppressWarnings("serial")
public class MainWindowFrame extends JFrame {
	private final DisplayFruitPanel fruitImagesPanel;
	private final FruitCreationPanel createFruitPanel;

	public MainWindowFrame(FruitBasketController basket, DisplayModel affichage) {
		super("Demonstration du GUI avec Afficheur");
		setLayout(new BorderLayout());

		fruitImagesPanel = new DisplayFruitPanel(basket, affichage);
		add(fruitImagesPanel, BorderLayout.CENTER);

		createFruitPanel = new FruitCreationPanel(basket, fruitImagesPanel);
		add(createFruitPanel, BorderLayout.WEST);

		setPreferredSize(new Dimension(800, 400));
		setLocation(300, 200);

		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
