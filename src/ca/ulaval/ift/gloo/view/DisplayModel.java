package ca.ulaval.ift.gloo.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;
import ca.ulaval.ift.gloo.guidemo.model.Strawberry;

public class DisplayModel {
	private static final String APPLE_IMAGE_PATH = "images/apple.png";
	private static final String BANANA_IMAGE_PATH = "images/banana.png";
	private static final String IMAGE_STRAWBERRY_PATH = "images/strawberry.png";
	private static final int BORDER_SIZE = 10;

	private static final int APPLE_INITIAL_Y_POSITION = 0;
	private static final int BANANA_INITIAL_Y_POSITION = 100;

	private static final int Y_STEP_SIZE = 10;
	private static final int X_STEP_SIZE = 50;

	private Image appleImage = null;
	private Image bananaImage = null;
	private Image strawberryImage = null;

	public DisplayModel() {
		try {
			appleImage = ImageIO.read(new File(APPLE_IMAGE_PATH));
			bananaImage = ImageIO.read(new File(BANANA_IMAGE_PATH));
			strawberryImage = ImageIO.read(new File(IMAGE_STRAWBERRY_PATH));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void paintModel(Graphics g, FruitBasketController basket) {
		char[] message = "Click in the window!".toCharArray();
		g.setFont(g.getFont().deriveFont(20.0f));
		g.drawChars(message, 0, message.length, 100, 40);
		
		if (appleImage != null && bananaImage != null) {
			int x = BORDER_SIZE;
			int y = BORDER_SIZE + APPLE_INITIAL_Y_POSITION;
			for (int i = 0; i < basket.getApples().size(); i++) {
				g.drawImage(appleImage, x, y, null);
				x += X_STEP_SIZE;
				y += Y_STEP_SIZE;
			}
			x = BORDER_SIZE;
			y = BORDER_SIZE + BANANA_INITIAL_Y_POSITION;
			for (int i = 0; i < basket.getBananas().size(); i++) {
				g.drawImage(bananaImage, x, y, null);
				x += 50;
				y += 10;
			}

			for (Strawberry s : basket.getStrawberries()) {
				int sbX = s.getX() - strawberryImage.getWidth(null) / 2;
				int sbY = s.getY() - strawberryImage.getHeight(null) / 2;
				g.drawImage(strawberryImage, sbX, sbY, null);
			}
		}
	}
}
