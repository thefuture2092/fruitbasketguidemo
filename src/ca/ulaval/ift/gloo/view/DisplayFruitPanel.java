package ca.ulaval.ift.gloo.view;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;

public class DisplayFruitPanel extends JPanel implements MouseInputListener {
	private static final long serialVersionUID = 1L;

	private final DisplayModel afficheur;
	private final FruitBasketController basket;

	public DisplayFruitPanel(FruitBasketController basket, DisplayModel affichage) {
		this.afficheur = affichage;
		this.basket = basket;
		
		this.addMouseListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		afficheur.paintModel(g, basket);
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		basket.addStrawberry(event.getPoint().x, event.getPoint().y);
		this.repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
