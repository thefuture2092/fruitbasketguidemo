package ca.ulaval.ift.gloo.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;

@SuppressWarnings("serial")
public class FruitCreationPanel extends JPanel implements ActionListener {
	private static final String ADD_FRUIT_STRING = "Add Fruit";
	private static String APPLE_STRING = "Apple";
	private static String BANANA_STRING = "Banana";

	private final JSpinner spinner;
	private final JRadioButton appleButton;
	private final JRadioButton bananaButton;
	private final JButton addFruitButton;

	private final FruitBasketController basket;
	private final DisplayFruitPanel displayPanel;

	public FruitCreationPanel(FruitBasketController basket, DisplayFruitPanel displayPanel) {
		this.basket = basket;
		this.displayPanel = displayPanel;

		setLayout(new GridLayout(8, 1));
		setPreferredSize(new Dimension(200, 400));
		SpinnerModel quantityModel = new SpinnerNumberModel(1, // initial value
				1, // min
				100, // max
				1); // step
		spinner = addLabeledSpinner(this, "Quantity", quantityModel);
		spinner.setEditor(new JSpinner.NumberEditor(spinner, "#"));

		appleButton = new JRadioButton(APPLE_STRING);
		appleButton.setMnemonic(KeyEvent.VK_A);
		appleButton.setActionCommand(APPLE_STRING);
		appleButton.setSelected(true);

		bananaButton = new JRadioButton(BANANA_STRING);
		bananaButton.setMnemonic(KeyEvent.VK_B);
		bananaButton.setActionCommand(BANANA_STRING);

		// Group the radio buttons.
		ButtonGroup group = new ButtonGroup();
		group.add(appleButton);
		group.add(bananaButton);

		// Register a listener for the radio buttons.
		appleButton.addActionListener(this);
		bananaButton.addActionListener(this);

		// Put the radio buttons in a column in a panel.
		JPanel radioPanel = new JPanel(new GridLayout(0, 1));
		radioPanel.add(appleButton);
		radioPanel.add(bananaButton);

		add(radioPanel);

		addFruitButton = new JButton(ADD_FRUIT_STRING);
		addFruitButton.addActionListener(this);
		add(addFruitButton);
	}

	static protected JSpinner addLabeledSpinner(Container c, String label, SpinnerModel model) {
		JLabel l = new JLabel(label);
		c.add(l);

		JSpinner spinner = new JSpinner(model);
		l.setLabelFor(spinner);
		c.add(spinner);

		return spinner;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(ADD_FRUIT_STRING)) {
			addFruits();
			displayPanel.repaint();
		}
	}

	private void addFruits() {
		Integer numberToCreate = (Integer) spinner.getModel().getValue();
		if (appleButton.isSelected()) {
			basket.addApples(numberToCreate);
		} else {
			basket.addBananas(numberToCreate);
		}
	}
}
