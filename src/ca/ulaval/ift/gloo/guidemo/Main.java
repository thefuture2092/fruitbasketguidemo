package ca.ulaval.ift.gloo.guidemo;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;
import ca.ulaval.ift.gloo.view.DisplayModel;
import ca.ulaval.ift.gloo.view.MainWindowFrame;

/**
 * 
 * @author Mathieu Dumoulin
 *
 */
public class Main {
	public static void main(String[] args) {
		new MainWindowFrame(new FruitBasketController(), new DisplayModel());
	}
}
