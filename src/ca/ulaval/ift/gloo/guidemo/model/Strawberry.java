package ca.ulaval.ift.gloo.guidemo.model;

public class Strawberry implements Fruit {
	public Strawberry(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	private int x;
	private int y;

}
